package com.example.databaseactivity.DatabaseHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.databaseactivity.ModelClass.APiModel;
import com.example.databaseactivity.ModelClass.DataModel;

import java.util.ArrayList;
import java.util.List;

public class DbHelper extends SQLiteOpenHelper {
    private static String DATABASE_NAME="DemoData.db";
    private static int DATABASE_VERSION=1;
    private static String TABLE_NAME="Employee";
    private static String TABLE_NAME_API="APITable";
    public DbHelper( Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE="CREATE TABLE "+TABLE_NAME+
                "( id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name TEXT , department TEXT , salary INTEGER );";
        db.execSQL(CREATE_TABLE);
        Log.d("Database_Created", "onCreate: SUcess");


        String CREATE_TABLE_API="create table "+TABLE_NAME_API+
                "( userId INTEGER,id INTEGER,title TEXT,body TEXT);";
        db.execSQL(CREATE_TABLE_API);
        Log.d("Database Api Created", "onCreate: SUcess Api");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop  table if exists "+TABLE_NAME);
        db.execSQL("drop  table if exists "+TABLE_NAME_API);
        onCreate(db);
    }

    public void AddingData(String name,String department,int salary) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("department", department);
        values.put("salary", salary);
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public List<DataModel> GetAllData(){
        SQLiteDatabase db=this.getWritableDatabase();
        List<DataModel> dataModels=new ArrayList<>();
        String query="select * from "+TABLE_NAME;
        Cursor cursor=db.rawQuery(query,null);

        while (cursor.moveToNext()){
            DataModel dataModel=new DataModel();
            dataModel.setName(cursor.getString(0));
            dataModel.setName(cursor.getString(1));
            dataModel.setName(cursor.getString(2));
            dataModels.add(dataModel);
        }
        return dataModels;
    }

    public int GetAllDataCount(){
        SQLiteDatabase db=this.getReadableDatabase();
        String query="Select * from "+TABLE_NAME;
        Cursor cursor=db.rawQuery(query,null);
        return cursor.getCount();
    }

    public void AddingDataToAPiTable(List<APiModel> list){
        SQLiteDatabase db = this.getWritableDatabase();
        for(int i=0;i< list.size();i++){
            ContentValues values = new ContentValues();
            values.put("userId",list.get(i).getUserId());
            values.put("id",list.get(i).getId());
            values.put("title",list.get(i).getTitle());
            values.put("body",list.get(i).getBody());
            db.insert(TABLE_NAME_API, null, values);
            db.close();
        }
    }

    public ArrayList<APiModel> GettingDataFromApiTable(){
        SQLiteDatabase db=this.getReadableDatabase();
        String query="Select * from "+TABLE_NAME_API;
        ArrayList<APiModel> modelList=new ArrayList<>();
        Cursor cursor=db.rawQuery(query,null);
        while (cursor.moveToNext()){
            APiModel aPiModel=new APiModel();
            aPiModel.setUserId(cursor.getInt(0));
            aPiModel.setId(cursor.getInt(1));
            aPiModel.setTitle(cursor.getString(2));
            aPiModel.setBody(cursor.getString(3));
            modelList.add(aPiModel);
        }
        return modelList;

    }

}
