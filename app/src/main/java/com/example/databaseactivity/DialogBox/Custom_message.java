package com.example.databaseactivity.DialogBox;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.databaseactivity.R;

public class Custom_message extends Dialog {

    TextView tv;
    Button okyBtn;
    public Custom_message(@NonNull Context context) {
        super(context);
        setContentView(R.layout.custom_message);
//        tv=findViewById(R.id.sucessId);
//        okyBtn=findViewById(R.id.okayBtn);
        SharedPreferences preferences= context.getSharedPreferences("MessageBox",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferences.edit();
        editor.putBoolean("Show",false);
        editor.apply();
        setCancelable(true);
//        okyBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(context, "Okay", Toast.LENGTH_SHORT).show();
//                dismiss();
//            }
//        });
    }
}
