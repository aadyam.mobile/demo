package com.example.databaseactivity.RetrofitPackage;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UploadApis {
    //Single Image
    @Multipart
    @POST("upload")
    Call<ResponseBody> upload(@Part MultipartBody.Part part,
                             @Part("SomeData") RequestBody requestBody);

    @Multipart
    @POST("uploadMultiple")
    Call<ResponseBody> uploadMultiple(@Part MultipartBody.Part part1,
                                     @Part MultipartBody.Part part2);

}
