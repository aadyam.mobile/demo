package com.example.databaseactivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.databaseactivity.AdapterPack.ApiAdapter;
import com.example.databaseactivity.DatabaseHelper.DbHelper;
import com.example.databaseactivity.DialogBox.Custom_message;
import com.example.databaseactivity.ModelClass.APiModel;
import com.example.databaseactivity.ModelClass.DataModel;
import com.example.databaseactivity.RetrofitPackage.NetWorkUtils;
import com.example.databaseactivity.RetrofitPackage.UploadApis;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.http.Multipart;

public class MainActivity extends AppCompatActivity {

    EditText nameText, departmentText, salaryText;
    Button submit_Btn, insertBtn, ShowBtn, GetForomApi, database, selectBtn;
    DbHelper db;
    TextView answer;
    LinearLayout first, second;
    ImageView selectedImage;
    RecyclerView recyclerView;
    int SELECT_PICTURE = 200;
    Custom_message custom_message;
    String filepath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameText = findViewById(R.id.text_input_name);
        departmentText = findViewById(R.id.text_input_department);
        salaryText = findViewById(R.id.text_input_salary);
        submit_Btn = findViewById(R.id.submit_btn);
        insertBtn = findViewById(R.id.InsertData);
        ShowBtn = findViewById(R.id.ShowDate);
        first = findViewById(R.id.firstLinear);
        second = findViewById(R.id.secondLinear);
        GetForomApi = findViewById(R.id.GetDataFromAPiI);
        answer = findViewById(R.id.answerAPI);
        database = findViewById(R.id.fromdatabse);
        recyclerView = findViewById(R.id.recycler);
        selectBtn = findViewById(R.id.BSelectImage);
        selectedImage = findViewById(R.id.IVPreviewImage);

        db = new DbHelper(this);

        second.setVisibility(View.VISIBLE);

        SharedPreferences preferences = getSharedPreferences("MessageBox", MODE_PRIVATE);
        boolean MessageShow = preferences.getBoolean("Show", true);
        if (MessageShow) {
            customDialog();
        }


        selectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent();
                    i.setType("image/*");
                    i.setAction(Intent.ACTION_GET_CONTENT);

                    // pass the constant to compare it
                    // with the returned requestCode
                    startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE);
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }

            }
        });

        insertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TabActivity.class);
                startActivity(intent);
//                PostDataUsingAPI();
//                second.setVisibility(View.VISIBLE);
            }
        });
        ShowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                second.setVisibility(View.GONE);
                db.GetAllDataCount();
                GetDataFromAPIUsingHttp();
                Log.d("Count", "onClick: " + db.GetAllDataCount());

            }
        });
        GetForomApi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                db=new DbHelper(getApplicationContext());
                answer.setVisibility(View.VISIBLE);
                GetDataFromAPIUsingLibray();
            }
        });
        submit_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(nameText.getText().toString())) {
                    nameText.setError("Please Enter Name");
                } else if (TextUtils.isEmpty(departmentText.getText().toString())) {
                    departmentText.setError("Please Enter Department");
                } else if (TextUtils.isEmpty(salaryText.getText().toString())) {
                    salaryText.setError("Please Enter salary");
                } else {
                    //All Are Clear
                    db.AddingData(nameText.getText().toString(),
                            departmentText.getText().toString(),
                            Integer.parseInt(salaryText.getText().toString()));
                    Log.d("Insert_Data_In_Employee", "onClick: Insert Data True");
                }

            }
        });
        database.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answer.setVisibility(View.INVISIBLE);
                Intent intent = new Intent(getApplicationContext(), NewActivity.class);
                startActivity(intent);
                getfromDatabase();
            }
        });

        uploadImage();
        uploadMultipleImage();;
    }

    ///Upload Single Iamge
    private void uploadImage() {
        File file = new File(filepath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part mulit1 = MultipartBody.Part.createFormData("newImage", file.getName(), requestBody);

        RequestBody requestBody1 = RequestBody.create(MediaType.parse("text/plain"), "This is New Image");

        Retrofit retrofit = NetWorkUtils.getRetrofit();
        UploadApis apis = retrofit.create(UploadApis.class);

        retrofit2.Call call = apis.upload(mulit1, requestBody1);
        String TAG = "UploadImage";

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, retrofit2.Response response) {
                Log.d(TAG, "onResponse: ");
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }

    ///Upload Multiple Image
    private MultipartBody.Part prepareImage(String path, String partname) {
        File file = new File(path);
        RequestBody request = RequestBody.create(
                MediaType.parse(getContentResolver().getType(Uri.fromFile(file))), file);
        return MultipartBody.Part.createFormData(partname, file.getName(), request);
    }

    private void uploadMultipleImage() {
        MultipartBody.Part image1 = prepareImage(filepath, "image1");
        MultipartBody.Part image2 = prepareImage(filepath, "image2");
        Retrofit retrofit = NetWorkUtils.getRetrofit();
        UploadApis Uploadapis = retrofit.create(UploadApis.class);
        retrofit2.Call<ResponseBody> uploader = Uploadapis.uploadMultiple(image1, image2);
        String TAG="Multiple Image Upload";
        uploader.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                Log.d(TAG, "onResponse: Great Sucess");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure: Failed");
            }
        });

    }

    //////////////////////////////
    private void customDialog() {
        custom_message = new Custom_message(this);
        custom_message.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        custom_message.show();
//        custom_message.setCancelable(false );
    }

    private void GetDataFromAPIUsingHttp() {
        String url = "https://jsonplaceholder.typicode.com/posts";
        String response = "";
        try {
            URL url1 = new URL(url);
            HttpURLConnection httpURLConnection = null;
            httpURLConnection = (HttpURLConnection) url1.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//            BufferedReader bis=new BufferedReader(inputStreamReader);
////            String line = "";
//            while ((line = bis.readLine()) != null){
//                response=response+line;
//            }
            int data = inputStreamReader.read();
            while (data != -1) {
                char current = (char) data;
                data = inputStreamReader.read();
                System.out.print(current);
                Log.d("ResponseHttp", "GetDataFromAPIUsingHttp: " + current);
            }
//            Log.d("ResponseHTTp", "GetDataFromAPIUsingHttp: "+response);
//            if(!response.isEmpty()){
//                JSONObject jsonObject=new JSONObject(response);
//                JSONArray jsonArray=new JSONArray(jsonObject);
//                Log.d("GetDataFromApiUsingH", "GetDataFromAPIUsingHttp: "+jsonArray.toString());
//            }


        } catch (Exception e) {

        }
    }

    private void getfromDatabase() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<APiModel> listApi = new ArrayList<>();
        listApi = db.GettingDataFromApiTable();
        ApiAdapter adapter = new ApiAdapter(this, listApi);
        recyclerView.setAdapter(adapter);
//        answer.setText(listApi.toString());
        Log.d("getfromDatabase", "getfromDatabase: " + listApi);
    }

    private void GetDataFromAPIUsingLibray() {

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://jsonplaceholder.typicode.com/posts";
        //Create String Request
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        List<APiModel> list = new ArrayList<>();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int userid = Integer.parseInt(jsonObject.getString("userId"));
                        int id = Integer.parseInt(jsonObject.getString("id"));
                        String title = jsonObject.getString("title");
                        String body = jsonObject.getString("body");

                        APiModel aPiModel = new APiModel(userid, id, title, body);
                        list.add(aPiModel);
                        db.AddingDataToAPiTable(list);
                        Log.d("ObjectwiseList", "onResponse: " + list);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                answer.setText(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("#Error", "onResponse: " + error);
            }
        });
        queue.add(request);
    }


    private void PostDataUsingAPI() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://reqres.in/api/users";
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d("ResponsePostAPI", "onResponse: " + jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Response Error", "onErrorResponse: " + error.toString());
            }
        }) {
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> param = new HashMap<String, String>();
                param.put("name", "morpheus");
                param.put("job", "leader");
                return param;
            }
        };
        queue.add(request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            // compare the resultCode with the
            // SELECT_PICTURE constant
            if (requestCode == SELECT_PICTURE) {
                // Get the url of the image from data
                Uri selectedImageUri = data.getData();
                Log.d("selectedImageUrl", "onActivityResult: " + selectedImageUri.toString());
                if (null != selectedImageUri) {
                    // update the preview image in the layout
                    selectedImage.setImageURI(selectedImageUri);
                }
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences preferences = getSharedPreferences("MessageBox", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("Show", true);
        editor.apply();
    }
}