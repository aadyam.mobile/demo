package com.example.databaseactivity.AdapterPack;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.databaseactivity.ModelClass.APiModel;
import com.example.databaseactivity.R;

import java.util.ArrayList;

public class ApiAdapter extends RecyclerView.Adapter<ApiAdapter.viewHOlder> {
    Context mContext;
    ArrayList<APiModel> list;

    public ApiAdapter(Context mContext, ArrayList<APiModel> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @NonNull
    @Override
    public ApiAdapter.viewHOlder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_answer,parent,false);
        return new viewHOlder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ApiAdapter.viewHOlder holder, int position) {
        holder.user.setText(String.valueOf(list.get(position).getUserId()));
        holder.id.setText(String.valueOf(list.get(position).getId()));
        holder.title.setText( list.get(position).getTitle());
        holder.body.setText( list.get(position).getBody());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class viewHOlder extends RecyclerView.ViewHolder {
        TextView user,id,title,body;
        public viewHOlder(@NonNull View itemView) {
            super(itemView);
            user=itemView.findViewById(R.id.userIdText);
            id=itemView.findViewById(R.id.IdText);
            title=itemView.findViewById(R.id.TitleText);
            body=itemView.findViewById(R.id.bodyText);
        }
    }
}
