package com.example.databaseactivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.databaseactivity.FragmentPackage.Fragment1;
import com.example.databaseactivity.FragmentPackage.Fragment2;
import com.example.databaseactivity.FragmentPackage.Fragment3;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class TabActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    TabHandllerAdapter tabHandllerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        tabLayout=findViewById(R.id.tablayot);
        viewPager=findViewById(R.id.viewpager);
        tabHandllerAdapter=new TabHandllerAdapter(getSupportFragmentManager());
        tabHandllerAdapter.AddFragment(new Fragment1(),"One");
        tabHandllerAdapter.AddFragment(new Fragment2(),"Two");
        tabHandllerAdapter.AddFragment(new Fragment3(),"Three");
        viewPager.setAdapter(tabHandllerAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }
    private class TabHandllerAdapter extends FragmentPagerAdapter{

        ArrayList<Fragment> fragmentArrayList=new ArrayList<>();
        ArrayList<String> stringArrayList=new ArrayList<>();

        public void AddFragment(Fragment f,String s){
            fragmentArrayList.add(f);
            stringArrayList.add(s);
        }

        public TabHandllerAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragmentArrayList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentArrayList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return stringArrayList.get(position);
        }
    }
}